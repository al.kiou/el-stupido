import java.util.ArrayList;

class User{

  private String username;
  private String name;
  private String surname;
  private String passwordHash;
  private boolean loggedin = false;

  public User(String username, String passwordHash){
    this.username = username;
    this.passwordHash = passwordHash;
  }

  public String getUsername(){
    return username;
  }

  //
  public boolean verifyPassword(String password){
    if (password.equals(passwordHash)){
      loggedin = true;
      return true;
    }else
      return false;
  }

  public void changePassword(String old, String newp) throws Exception{
    if (old.equals(passwordHash))
      passwordHash = newp;
    else
      throw new Exception("falsches Passwort");
  }
  @Override
  public String toString(){
    return username;
  }

}// class User


class Secretary extends User{

  public Secretary(String user, String passwordHash){
    super(user,passwordHash);
  }

}// class Secretary


class Teacher extends User{

  private ArrayList<Course> coursesTeaching = new ArrayList<Course>();

  public Teacher(String user, String passwordHash){
    super(user,passwordHash);
  }

  // returns list of Courses teaching
  public ArrayList<Course> coursesTeaching(){
    return coursesTeaching;
  }

  // add new Course to teach
  public void addCourse(Course course){
    coursesTeaching.add(course);
    course.setTeacher(this);
  }

}// class Teacher


class Student extends User{

  private ArrayList<Course> coursesAttending = new ArrayList<Course>();
  private ArrayList<Grade> grades = new ArrayList<Grade>();

  public Student(String user, String passwordHash){
    super(user,passwordHash);
  }

  // add new Course to the Registration
  public void addCourse(Course course){
    coursesAttending.add(course);
  }

  // returns list of Courses registered
  public ArrayList<Course> coursesAttending(){
    return coursesAttending;
  }

  // add new grade (for Secretary)
  public void addGrade(Course course, int grade){
    try{
    grades.add(new Grade(course,grade));
  }catch(Exception e){System.out.println(e);}
  }
  public void addGrade(Grade grade){
    grades.add(grade);
  }

  // returns all the Courses the Student is registered to
  public String registration(){
    String s = "";
    for (int i=0; i<coursesAttending.size(); i++){
      s += coursesAttending.get(i);
      s += "\n";
    }
    return s;
  }

  // returns all the Student's grades
  public String grades(){
    String s="";
    for (int i=0; i<coursesAttending.size(); i++){
      s += grades.get(i);
      s += "\n";
    }
    return s;
  }

  @Override
  public String toString(){
    return (this.getUsername() +"\nRegistrierung:\n"  +this.registration() +"\nNoten:\n" +this.grades());
  }
}// class Student
