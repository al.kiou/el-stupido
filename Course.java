import java.util.ArrayList;

class Course{

  private String code;
  private String title;
  private Course prerequisite;
  private Teacher teacher;
  private ArrayList<Student> students = new ArrayList<Student>();

  public Course(String code, String title){
    this.code = code;
    this.title = title;
  }
  public Course(String code, String title, Teacher teacher){
    this.code = code;
    this.title = title;
    this.teacher = teacher;
  }
  public Course(String code, String title, Course prerequisite){
    this.code = code;
    this.title = title;
    this.prerequisite = prerequisite;
  }
  public Course(String code, String title, Course prerequisite, Teacher teacher){
    this.code = code;
    this.title = title;
    this.teacher = teacher;
    this.prerequisite = prerequisite;
  }

  public String getCode(){
    return code;
  }

  public String getTitle(){
    return title;
  }

  public Course getPrerequisite(){
    return prerequisite;
  }

  public Teacher getTeacher(){
    return teacher;
  }

  public void setTeacher(Teacher teacher){
    this.teacher = teacher;
  }

  public void setPrerequisite(Course prerequisite){
    this.prerequisite = prerequisite;
  }

  // add a new Student to the Course
  public void addStudent(Student student){
    students.add(student);
  }

  // call the toString function of every Student having attended the Course
  public void printStudents(){
    for (int i=0; i<students.size(); i++)
      System.out.println(students.get(i));
  }

  public ArrayList<Student> students(){
    return students;
  }

  // return Student object with the username given
  public Student findByUsername(String username){
    for (int i=0; i<students.size(); i++)
      if (students.get(i).getUsername().equals(username))
        return students.get(i);
    return null;
  }

  @Override
  public String toString(){
    String courseTitle = "-";
    String teacherName = "-";
    if(prerequisite != null)
      courseTitle = prerequisite.getTitle();
    if(teacher != null)
      teacherName = teacher.getUsername();
    return ("Code: " +code +", Titel: " +title +", Voraussetzung: " +courseTitle +", Lehrer: " +teacherName +"\n");
  }

}
