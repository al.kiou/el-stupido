class Grade{

  private Course course;
  private int grade;

  public Grade(Course course, int grade) throws Exception{
    if (grade <0 || grade >10)
      throw new Exception("Die Note muss zwischen 0 und 10 liegen");
    this.course = course;
    this.grade = grade;
  }

  @Override
  public String toString(){
    return ("Kurs: " +course.getTitle() +" Note: " +String.valueOf(grade));
  }

}
