import java.util.*;

class DaphneChewer{

  private static User loggedUser; // instance of the user served by the chewer

  private static ArrayList<User> users = new ArrayList<User>();

  private static ArrayList<Course> courses = new ArrayList<Course>();

  private static Scanner input = new Scanner(System.in); // well, Java...




  public static void main (String[] args){

    addStuff(); // adds some users and courses for testing

    String choice = "Meh";

    // Login - Registration Menu
    while(true){
      print("1. Anmeldung - Login");
      print("2. registrieren - register");
      print("0. Ausfahrt - exit");
      choice = input.next();
      print("choice "+choice);
      if(choice.equals("1")){
        while (loggedUser == null){
          loggedUser = login(); // function verifies crendentials and returns instance of the User
        }
      }else if (choice.equals("2")){
        loggedUser = register(); // function verifies crendentials and returns instance of the User
      }else if (choice.equals("0")){
        print("Auf Wiedersehen!");
        System.exit(0);
      }else{
        print("Ich werde jetzt abstürzen");
        print("(English) I will crash now");
      }

      // Class of user is determined and the corresponding function for handling the user actions is called
      String userClass = loggedUser.getClass().getSimpleName();
      if (userClass.equals("Student"))
        student();
      if (userClass.equals("Secretary"))
        secretary();
      if (userClass.equals("Teacher"))
        teacher();
    }// while

  }// main

  private static void addStuff(){
    users.add(new Student("s","s"));
    users.add(new Secretary("se","se"));
    users.add(new Teacher("t","t"));

    courses.add(new Course("0","Programming"));
    courses.add(new Course("1","Web"));
    courses.add(new Course("2","Electronics"));

    Teacher teacher = (Teacher)users.get(2);
    teacher.addCourse(courses.get(1));

    Student student = (Student)users.get(0);
    courses.get(1).addStudent(student);
  }


  private static void student(){

    Student loggedStudent = (Student)loggedUser; // cast to daughter class

    int choice = -1;

    while(choice != 0){
      print("\n");
      print("1. Profil anzeigen - View Profile");
      print("2. Registrierung anzeigen - View Registration");
      print("3. Noten anzeigen - View Grades");
      print("4. neue Anfrage - New Request");
      print("0. Ausloggen - logout");
      choice = Integer.parseInt(input.next()); // CHOOOOOOSE

      switch(choice){
        case 0:
          loggedUser = null;
        break;
        case 1:
          print("Nutzername: " +loggedStudent.getUsername());
        break;
        case 2:
          print("Registrierung: \n" +loggedStudent.registration());
        break;
        case 3:
          print("Noten: \n" +loggedStudent.grades());
        break;
        case 4:
          print("Bauarbeiten im Gange");
        break;
        default:
          print("ungültige Option");
        break;
      }// switch
    }// while
  }// student()

  private static void teacher(){

    Teacher loggedTeacher = (Teacher)loggedUser; // cast to daughter class

    int choice = -1;

    while(choice != 0){
      print("\n");
      print("1. Profil anzeigen - View Profile");
      print("2. Meine Kurse anzeigen - View My Courses");
      print("3. Festlegen der Schülernote - Set Student Grade");
      print("0. Ausloggen - logout");
      choice = Integer.parseInt(input.next()); // CHOOOOOOSE

      switch(choice){
        case 0:
          loggedUser = null;
        break;
        case 1:
          print("Bauarbeiten im Gange");
        break;
        case 2:
          printArrayList(loggedTeacher.coursesTeaching()); // print teacher's courses
        break;
        case 3:
          print("Bauarbeiten im Gange");
          printArrayList(loggedTeacher.coursesTeaching()); // print teacher's courses
          print("Kurscode wählen - Choose Course Code");
          Course course = courses.get(getIndexOfCourse(input.next())); // get course by code
          print("Studenten");
          course.printStudents(); // print students attending course
          print("Wähle einen Schüler - Choose Student");
          Student student = (Student)users.get(getIndexOfUser(input.next())); // get student by username
          print("Note einstellen - Set Grade");
          student.addGrade(course, Integer.parseInt(input.next())); // add grade
        break;
        default:
          print("ungültige Option");
        break;
      }// switch
    }// while
  }// teacher()

  private static void secretary(){

    Secretary loggedSecretary = (Secretary)loggedUser; // cast to daughter class

    int choice = -1;

    while(choice != 0){
      print("\n");
      print("1. Profil anzeigen - View Profile");
      print("2. Kurse anzeigen - View Courses");
      print("3. Kurs hinzufügen - Add Course");
      print("4. Kurs löschen - Delete Course");
      print("5. Voraussetzung hinzufügen Kurs - Add prerequisite Course");
      print("6. Lehrer zuweisen - Assign Teacher");
      print("7. Benutzer hinzufügen - Add Users");
      print("8. Anmeldedatum festlegen - Set Registration Validate");
      print("0. Ausloggen - logout");
      choice = Integer.parseInt(input.next());

      switch(choice){
        case 0:
          loggedUser = null;
        break;
        case 1: // view profile
          print("Nutzername: " +loggedSecretary.getUsername());
        break;
        case 2: // print courses
          printArrayList(courses);
        break;
        case 3: // add course
          System.out.print("Kurscode: ");
          String code = input.next(); // input code
          System.out.print("Kursname: ");
          addCourse(code, input.next()); // add course with given code and name input
          print("Kurs hinzugefügt - Course Added");
        break;
        case 4: // delete course
          print("Wählen Sie den zu löschenden Code");
          print("(English) Select code of course to be deleted");
          printArrayList(courses); // print all courses
          System.out.print("Kurscode: ");
          deleteCourse(input.next()); // ddelete course by code
        break;
        case 5:
          print("Kurs auswählen, um eine Voraussetzung hinzuzufügen");
          print("(English) Choose course to add a prerequisite");
          printArrayList(courses); // print all courses
          System.out.print("Kurscode: ");
          String courseCode = input.next(); // course to add prerequisite
          System.out.print("Voraussetzung: ");
          String prerequisiteCode = input.next(); // prerequisite to be added
          courses.get(getIndexOfCourse(courseCode)).setPrerequisite(courses.get(getIndexOfCourse(prerequisiteCode))); // hard to explain
        break;
        case 6:
          print("Liste der Lehrer - List of Teachers");
          for(int i = 0; i < users.size(); i++){ // for every user
            if(users.get(i).getClass().getSimpleName().equals("Teacher")) // if its class name is "Teacher"
              print(users.get(i).getUsername()); // print his/her username
          }
        break;
        case 7:
          print("Bauarbeiten im Gange");
        break;
        case 8:
          print("Bauarbeiten im Gange");
        break;
        default:
          print("ungültige Option");
        break;
      }// switch
    }// while
  }// secretary


  // returns the instance of the User who does login
  private static User login(){
    int userIndex;
    String username;
    String password;

    // A welcoming message
    print("Hallo, lass uns anfangen, Daphne zu kauen!");
    print("(English) Hello, let's begin chewing some daphne!\n");
    print("Loggen Sie sich auf Wunsch in Ihr Konto ein.");
    print("(Engilsh) Login to your account if you wish. \n");


    do{
      // reading the username
      System.out.print("Nutzername: ");
      username = input.next();
      userIndex = userIndex(username);
    }while(userIndex == -1);
    // { // repaeat input until a existing username is entered
    //   print("\nEs gibt keinen solchen Benutzernamen, mein Freund.");
    //   print("(English) There is no such username my friend.");
    // }
    print("\nWillkommen " +username +".");
    System.out.print("Passwort: ");
    password = input.next();
    if (users.get(userIndex).verifyPassword(password)){
      print("Passwort richtig");
      return users.get(userIndex);
    }

    return null;

  }

  // assisting in login()
  private static int userIndex(String username){
    for(int i=0; i<users.size(); i++)
      if(users.get(i).getUsername().equals(username))
        return i;
    return -1;
  }

  private static User register(){
    print("Hallo neuer Benutzer, bitte geben Sie Ihre Zugangsdaten an");
    print("(English) Hello new User, please add your credentials");

    print("Nutzername: ");
    String username = input.next();

    print("Passwort: ");
    String password = input.next();

    User newUser = new Student(username, password); // create new user (Student)
    users.add(newUser); // and add to list
    print("Registrierung vervollständigt");

    return users.get(users.size() - 1);
  }

  // Well, fuck you Java, I am bored of typing System.out all day long
  private static void print(String s){
    System.out.println(s);
  }

  private static void printArrayList(ArrayList list){
    print(Arrays.toString(list.toArray()));
  }

  private static void addCourse(String code, String title){
    courses.add(new Course(code, title)); //add new Course to arrayList
  }

  private static void deleteCourse(String code){
    for(int i = 0; i < courses.size(); i++){ // for every course
      if(courses.get(i).getCode().equals(code)) // if its code is equal to the parameter
        courses.remove(i); // remove course from list
    }// for
  }

  private static int getIndexOfCourse(String code){
    for (int i = 0; i < courses.size(); i++)
      if (courses.get(i).getCode().equals(code))
        return i;
    return -1;
  }

  private static int getIndexOfUser(String username){
    for (int i = 0; i < users.size(); i++)
      if (users.get(i).getUsername().equals(username))
        return i;
    return -1;
  }

  private static int getIndexOfArrayList(ArrayList list, Object object){
    for (int i = 0; i < list.size(); i++)
      if (list.get(i).equals(object))
        return i;
    return -1;
  }

} // END OF CLASS
