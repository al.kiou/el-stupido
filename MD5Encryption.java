
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.util.Scanner;
import java.util.Arrays;


public class MD5Encryption {


    public static void main(String[] args) {
        String function;
        String text;
        String algorithm;
        if(args.length == 3){
            function = args[0];
            text = args[1];
            algorithm = args[2];
        }else{
            // Input the text and algorithm for the digestion
            Scanner sc = new Scanner(System.in);
            System.out.println("Digest or Validate? ");
            function = sc.nextLine().toLowerCase(); //Case Unsensitive
            System.out.println("Give a text message to digest/validate: ");
            text = sc.nextLine();
            System.out.println("Give the algorithm for digestion: ");
            algorithm = sc.nextLine().toUpperCase(); //Case Unsensitive
        }

        //Encrypt text and save to file
        if(function.equals("digest")){
            String hash = getHash(text,algorithm);
            writeFile(hash.getBytes(), "./hash.dat");
        }
        //Validate input Text compared to
        else if(function.equals("validate")){
            byte[] textBytes = text.getBytes(); //Input text in Bytes
            String hash = getHash(text,algorithm); //hash created by input text
            byte[] hashBytes = hash.getBytes(); //the created hash in bytes
            byte[] savedHashBytes = readFileToByteArray("./hash.dat"); //saved hash in bytes
            if(Arrays.equals(hashBytes, savedHashBytes))
                System.out.println("Match!");
            else
                System.out.println("Not a Match!");
        }
    }

  public static String getHash(String input, String algorithm){
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // translate the digested message to String.
            BigInteger bigInt = new BigInteger(1,messageDigest);
            String hashtext = bigInt.toString(16);

            System.out.println(hashtext); // Print the hashtext

            return hashtext;
        }catch (NoSuchAlgorithmException e) {
            System.out.println(e + "/nUse MD5 or SHA algorithm");
            return "";
        }
    }

  // Returns a file in byte array, file path given
  private static byte[] readFileToByteArray(String path){
    File file = new File(path);
    FileInputStream fis = null;
    // Creating a byte array using the length of the file
    // file.length returns long which is cast to int
    byte[] bytes = new byte[(int) file.length()];
    try{
      fis = new FileInputStream(file);
      fis.read(bytes);
      fis.close();
    }catch(IOException e){
      System.out.println("Exception: " + e);
    }
    return bytes;
  }

  // Creates a new File at the path given
  static void writeFile(byte[] bytes, String path) {
    File outputFile = new File(path); // File Object
    try {
      // Initialize a pointer
      // in file using OutputStream
      OutputStream os = new FileOutputStream(outputFile);
      os.write(bytes); // Starts writing the bytes in it
      os.close(); // Close the file
    }catch (Exception e) {
      System.out.println("Exception: " + e);
    }
  }

}
